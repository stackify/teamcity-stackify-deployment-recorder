package com.test.teamcity.testPlugin;

public class AppCommon {
    private final String RUNNER_TYPE = "deploymentRecorder";
    private final String DISPLAY_NAME = "Stackify Deployment Recorder";
    private final String DESCRIPTION = "Notify Stackify Retrace of your Deployment";
}
