package com.test.teamcity.testPlugin;

import jetbrains.buildServer.RunBuildException;
import jetbrains.buildServer.agent.AgentBuildRunner;
import jetbrains.buildServer.agent.AgentBuildRunnerInfo;
import jetbrains.buildServer.agent.AgentExtension;
import jetbrains.buildServer.agent.AgentRunningBuild;
import jetbrains.buildServer.agent.BuildAgentConfiguration;
import jetbrains.buildServer.agent.BuildProcess;
import jetbrains.buildServer.agent.BuildRunnerContext;
import org.jetbrains.annotations.NotNull;

public class StackifyRunner implements AgentExtension, AgentBuildRunner, AgentBuildRunnerInfo {

    private final String RUNNER_TYPE = "deploymentRecorder";

    @Override
    public BuildProcess createBuildProcess(
            @NotNull final AgentRunningBuild agentRunningBuild,
            @NotNull final BuildRunnerContext buildRunnerContext) throws RunBuildException {
       return new StackifyBuildProcess(agentRunningBuild, buildRunnerContext);
    }

    @Override
    public AgentBuildRunnerInfo getRunnerInfo() {
        return this;
    }

    @Override
    public String getType() {
        return RUNNER_TYPE;
    }

    @Override
    public boolean canRun(@NotNull final BuildAgentConfiguration buildAgentConfiguration) {
       return true;
    }
}
