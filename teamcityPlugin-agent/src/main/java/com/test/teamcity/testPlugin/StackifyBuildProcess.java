package com.test.teamcity.testPlugin;


import com.stackify.deployment.ApiDataService;
import com.stackify.deployment.ServiceLocator;
import jetbrains.buildServer.RunBuildException;
import jetbrains.buildServer.agent.*;
import jetbrains.buildServer.messages.BuildMessage1;
import jetbrains.buildServer.messages.Status;
import org.joda.time.DateTime;
import java.util.Calendar;
import java.util.Map;
import java.util.concurrent.*;

public class StackifyBuildProcess implements BuildProcess, Callable<BuildFinishedStatus>{
    private Future<BuildFinishedStatus> buildStatus;
    private final ExecutorService executor = Executors.newSingleThreadExecutor();

    private final AgentRunningBuild agent;
    private final BuildRunnerContext context;

    StackifyBuildProcess(final AgentRunningBuild agent, final BuildRunnerContext context) {
        this.agent = agent;
        this.context = context;
    }

    @Override
    public void start() throws RunBuildException {
        try {
            buildStatus = executor.submit(this);
        } catch (final RejectedExecutionException e) {
            throw new RunBuildException(e);
        }
    }

    public boolean isInterrupted() {
        return buildStatus.isCancelled() && buildStatus.isDone();
    }

    public boolean isFinished() {
        return buildStatus.isDone();
    }

    public void interrupt() {
        buildStatus.cancel(true);
    }

    @Override
    public BuildFinishedStatus waitFor() throws RunBuildException {
        try {
            final BuildFinishedStatus status = buildStatus.get();
           return status;
        } catch (final InterruptedException e) {
            throw new RunBuildException(e);
        } catch (final ExecutionException e) {
            throw new RunBuildException(e);
        } catch (final CancellationException e) {
            return BuildFinishedStatus.INTERRUPTED;
        } finally {
            executor.shutdown();
        }
    }

    public BuildFinishedStatus call() {
        try {

            ApiDataService apiService = ServiceLocator.getApiDataService();

            Map<String, String> params = context.getRunnerParameters();
            apiService.postDeploymentCompete(params.get("apiKey"), params.get("appName"), params.get("environment"), params.get("version"), params.get("name"), params.get("branch"), params.get("commit"), params.get("uri"));



            return BuildFinishedStatus.FINISHED_SUCCESS;
        } catch (Exception e) {
           /* log.log(Level.SEVERE, "Failed to record deployment in Stackify", e);
            listener.error("Failed to record deployment in Stackify");
            listener.error(e.getMessage());
            
            log.logMessage(new BuildMessage1("BuildFinishedStatus.call", "error", Status.ERROR, date, e));*/
            return BuildFinishedStatus.FINISHED_WITH_PROBLEMS;
        }
    }
}
