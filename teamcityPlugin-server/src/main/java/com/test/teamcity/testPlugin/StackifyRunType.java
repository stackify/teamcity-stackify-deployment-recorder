package com.test.teamcity.testPlugin;

import jetbrains.buildServer.serverSide.PropertiesProcessor;
import jetbrains.buildServer.serverSide.RunType;
import jetbrains.buildServer.serverSide.RunTypeRegistry;
import jetbrains.buildServer.web.openapi.PluginDescriptor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

public class StackifyRunType extends RunType {
    private PluginDescriptor descriptor;

    private final String RUNNER_TYPE = "deploymentRecorder";
    private final String DISPLAY_NAME = "Stackify Retrace Deployment Recorder";
    private final String DESCRIPTION = "Notify Stackify Retrace of your Deployment";

    public StackifyRunType(@NotNull final PluginDescriptor descriptor, @NotNull final RunTypeRegistry registry) {
        registry.registerRunType(this);
        this.descriptor = descriptor;
    }

    @NotNull
    @Override
    public String getType() {
        return RUNNER_TYPE;
    }

    @NotNull
    @Override
    public String getDisplayName() {
        return DISPLAY_NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public PropertiesProcessor getRunnerPropertiesProcessor() {
        return null;
    }

    @Nullable
    @Override
    public String getEditRunnerParamsJspFilePath() {
        return descriptor.getPluginResourcesPath("editSDRparams.jsp");
    }

    @Nullable
    @Override
    public String getViewRunnerParamsJspFilePath() {
        return descriptor.getPluginResourcesPath("viewSDRparams.jsp");
    }


    @Nullable
    @Override
    public Map<String, String> getDefaultRunnerProperties() {
        Map<String, String> defaults = new HashMap<String, String>();

        defaults.put("apiKey", "%system.Stackify.ApiKey%");
        defaults.put("environment", "%env.Stackify.Environment%");
        defaults.put("version", "%build.number%");
        defaults.put("name", "%env.TEAMCITY_PROJECT_NAME%");
        defaults.put("branch", "%vcsroot.branch%");
        defaults.put("commit", "%build.vcs.number%");
        defaults.put("uri", "%vcsroot.url%");

        return defaults;
    }

}
