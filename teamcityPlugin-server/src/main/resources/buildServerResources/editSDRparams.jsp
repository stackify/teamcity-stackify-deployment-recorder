<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>

<jsp:useBean id="propertiesBean" scope="request" type="jetbrains.buildServer.controllers.BasePropertiesBean"/>


<l:settingsGroup title="Stackify Retrace Deployment Recorder">
<tr>
<th>Api Key</th>
<td>
      <span>
        <props:textProperty name="apiKey" className="longField"/>
      </span>
      <span class="smallNote">Your Retrace Account Activation Key</span>
</td>
</tr>
<tr>
<th>Environment Name</th>
<td>
      <span>
        <props:textProperty name="environment" className="longField"/>
      </span>
      <span class="smallNote">Your Retrace Account Environment</span>
</td>
</tr>
<tr>
<th>App Name</th>
<td>
      <span>
        <props:textProperty name="appName" className="longField"/>
      </span>
      <span class="smallNote">Your Retrace App Name</span>
</td>
</tr>
<tr>
<th>Version</th>
<td>
      <span>
        <props:textProperty name="version" className="longField"/>
      </span>
      <span class="smallNote">Deployment version</span>
</td>
</tr>
<tr>
<th>Deployment Name</th>
<td>
      <span>
        <props:textProperty name="name" className="longField"/>
      </span>
      <span class="smallNote">Deployment Name</span>
</td>
</tr>
<tr>
<th>Branch</th>
<td>
      <span>
        <props:textProperty name="branch" className="longField"/>
      </span>
      <span class="smallNote">branch</span>
</td>
</tr>
<tr>
<th>Commit</th>
<td>
      <span>
        <props:textProperty name="commit" className="longField"/>
      </span>
      <span class="smallNote">Commit ID</span>
</td>
</tr>
<tr>
<th>URI</th>
<td>
      <span>
        <props:textProperty name="uri" className="longField"/>
      </span>
      <span class="smallNote">URL back to this deployment</span>
</td>
</tr>
</l:settingsGroup>