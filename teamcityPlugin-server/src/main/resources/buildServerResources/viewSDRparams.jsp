<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="parameter">
  Api Key: <props:displayValue name="apiKey" emptyValue="<empty>" showInPopup="true" popupTitle="ApiKey" popupLinkText="view api key"/>
</div>
<div class="parameter">
  Environment: <props:displayValue name="environment" emptyValue="<empty>" showInPopup="true" popupTitle="Environment" popupLinkText="view environment"/>
</div>
<div class="parameter">
  App Name: <props:displayValue name="appName" emptyValue="<empty>" showInPopup="true" popupTitle="AppName" popupLinkText="view app name"/>
</div>
<div class="parameter">
  Version: <props:displayValue name="version" emptyValue="<empty>" showInPopup="true" popupTitle="Version" popupLinkText="view version"/>
</div>
<div class="parameter">
  Deployment Name: <props:displayValue name="name" emptyValue="<empty>" showInPopup="true" popupTitle="Name" popupLinkText="view deployment name"/>
</div>
<div class="parameter">
  Branch: <props:displayValue name="branch" emptyValue="<empty>" showInPopup="true" popupTitle="Branch" popupLinkText="view branch"/>
</div>
<div class="parameter">
  Commit: <props:displayValue name="commit" emptyValue="<empty>" showInPopup="true" popupTitle="Commit" popupLinkText="view commit"/>
</div>
<div class="parameter">
  URI: <props:displayValue name="uri" emptyValue="<empty>" showInPopup="true" popupTitle="URL" popupLinkText="view url"/>
</div>